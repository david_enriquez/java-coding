package Practice.Arrays;

public class Arrays_007 {
    public static void main(String[] args) {
        //we declared an int array "numbers" with a
        //fixed size of 5 elements
        int[] numbers = new int[5];

        //for loop to add values 1 - 5 to "numbers" array
        //accessing each element will allow us to do so
        for (int i = 0; i < numbers.length; i++) {

            //assign each element with the 1 -5 values,
            //since we are accessing the elements based off of
            //their index, we do i + 1. this is basically current
            //index plus 1.
            numbers[i] = i + 1;

            //print "numbers" array at index through each iteration
            System.out.println(numbers[i]);
        }
    }
}
