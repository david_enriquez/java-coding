package Practice.Arrays;

public class Arrays_003 {
    public static void main (String [] args) {
        final int NUM_ELEMENTS = 4;
        int [] userVals = new int[NUM_ELEMENTS];
        int i;
        int maxVal;

        userVals[0] = 1;
        userVals[1] = 5;
        userVals[2] = 7;
        userVals[3] = 4;

        maxVal = userVals[0];
        for (i = 0; i < userVals.length; ++i) {
            System.out.println("for loop iteration = " + i);
           if (userVals[i] >= maxVal) {
              System.out.println(userVals[i]);
              maxVal = userVals[i];
              System.out.println(maxVal);
           }
        }
     }
}
