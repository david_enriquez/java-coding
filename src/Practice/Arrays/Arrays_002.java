package Practice.Arrays;

public class Arrays_002 {
    public static void main(String[] args) {
       int[] a = {1, 2, 3, 4};
       a[a.length - 1] = 10; // access last element assign 10
       System.out.println(a[0]);
       System.out.println(a[1]);
       System.out.println(a[2]);
       System.out.println(a[3]);
    }
}
