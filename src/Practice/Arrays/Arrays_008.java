package Practice.Arrays;
import java.util.Scanner;


// write a program that reads a list of integers,
// and outputs those integers in reverse. The input begins
// with an integer indicating the number of integers that
// follow. For coding simplicity, follow each output integer
// by a comma, including the last one. Assume that the list
// will always contain fewer than 20 integers.

// Ex: If the input is: 5 2 4 6 8 10
// the output is: 10,8,6,4,2,


public class Arrays_008 {
    public static void main(String[] args) {

        // create an instance of the Scanner class named scnr.
        // it is used to read input from the standard input stream
        // (usually the keyboard).
        Scanner scnr = new Scanner(System.in);

        // declares an array named userList that can hold up to 20 integers.
        int[] userList = new int[20];

        // declares an integer variable numElements, which will be used to
        // store the number of integers provided as input.
        int numElements;

        // this line uses the nextInt() method of the Scanner class to read
        // an integer from the user and store it in the numElements variable
        // this integer represents the number of integers that will follow
        numElements = scnr.nextInt();

        // this line starts a for loop that will iterate numElements times
        // it's used to read the list of integers provided by the user and
        // store them in the userList array
        for (int i = 0; i < numElements; i++) {
            // inside the loop, this line reads an integer from the user and
            // stores it in the i-th position of the userList array.
            userList[i] = scnr.nextInt();
            System.out.println("Number of elements is: " + numElements);
            System.out.println("Iterating through userList and printing current index: " + userList[i]);
         }
         // this line starts a for loop that iterates through the elements
         // of the userList array in reverse order it initializes i to
         // numElements - 1, which is the index of the last element in the array
         // it then decrements i by 1 in each iteration until i becomes
         // less than 0 (i.e., until it reaches the first element of the array)
         for (int i = numElements - 1; i >= 0; i--) {
            // inside the loop, this line prints the current element of the
            // userList array at index i since we're iterating in reverse order,
            // this effectively prints the elements of the array from
            // the last to the first.
            System.out.print(userList[i]);
            // this line begins an if statement to check whether the current
            // index i is greater than 0 this check is used to determine
            // whether or not to print a comma after the current element
            if (i >= 0) {
               System.out.print(",");
            }
         }
         System.out.println();

        // close the scanner
        scnr.close();
    }
}
