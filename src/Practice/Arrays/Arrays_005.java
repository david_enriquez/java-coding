package Practice.Arrays;

import java.util.Scanner;

public class Arrays_005 {
    public static void main (String [] args) {
      Scanner scnr = new Scanner(System.in);
      final int SCORES_SIZE = 4;
      int[] oldScores = new int[SCORES_SIZE];
      int[] newScores = new int[SCORES_SIZE];
      int i;
      for (i = 0; i < oldScores.length; ++i) {
         oldScores[i] = scnr.nextInt();
      }

      // my code is here under this comment
      for (i = 0; i < oldScores.length - 1; ++i) {

        //inside the loop, this line of code copies
        //the value of the element at index i + 1 from the
        //oldScores array to the newScores array at index i.
        //this effectively shifts the elements one position
        //to the left in newScores.
        newScores[i] = oldScores[i + 1];
        System.out.println("newScores element: " + newScores[i]);
        System.out.println("oldScores element: " + oldScores[i]);
      }
      // Set the last element in newScores to the first element in oldScores
      newScores[oldScores.length - 1] = oldScores[0];
      // my code ends here and is above this comment

      for (i = 0; i < newScores.length; ++i) {
         System.out.print(newScores[i] + " ");
      }
      System.out.println();
      scnr.close();
   }
}
