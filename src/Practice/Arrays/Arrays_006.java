package Practice.Arrays;
import java.util.Scanner;
import java.util.Arrays;

public class Arrays_006 {
    public static void main (String[] args) {
        Scanner scnr = new Scanner(System.in);
        String[] fantasyRoster = {
            "Joe Burrow",
            "Tony Pollard",
            "Davante Adams",
            "Michael Pittman",
        };
        String[] newFantasyRoster = Arrays.copyOf(fantasyRoster, fantasyRoster.length);

        newFantasyRoster[0] = "Patrick Mahomes";

        System.out.println("Original roster: ");
        for (String playerName : fantasyRoster) {
            System.out.println(playerName);
        }

        System.out.println("Modified roster: ");
        for (String playerName : newFantasyRoster) {
            System.out.println(playerName);
        }

        scnr.close();
    }
}
