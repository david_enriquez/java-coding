package Practice.TwoDArrays;
import java.util.Scanner;
import java.io.*;


// write a program that reads text from a file create a
// two-dimensional character array that is rows * columns fill
// the array as follows:
// on even number rows, fill the row from the leftmost column to the rightmost
// on odd number rows, fill the row from the rightmost column to the leftmost
// fill any unused spaces in the two-dimensional array with the
// " * " character, if you have more characters than space, ignore
// the excess characters
// extract the chars from the array in column-major order
// (pull from column 0 first, then column 1, etc.) build a new
// string as you extract the chars, display the new string
// if the input string is "And the rest is rust and stardust" the
// output should be "A rsnusuudisd trtt ahsateens rd" another example
// if the input is "And the rest is" the output should be
// "A **ns**di** **tt**hs**ee** r**"

public class TwoDArray_003 {
    public static void main(String[] args) {
        int rows = 4;
        int columns = 8;
        char[][] charArray = new char[rows][columns];
        Scanner scanner = new Scanner(System.in);
        try {
            scanner = new Scanner(new File("two_D_text.in"));
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String inputString = scanner.nextLine();
        int wordLen = inputString.length();
        int count = 0;
        for (int r = 0; r < rows; r++) {
            int cR = columns - 1;
            for (int c = 0; c < columns; c++) {
                if (r % 2 == 0 && count < wordLen) {
                    charArray[r][c] = inputString.charAt(count);
                }
                else if (r % 2 == 0 && count >= wordLen) {
                    charArray[r][c] = '*';
                }
                else if (r % 2 != 0 && count < wordLen) {
                    charArray[r][cR] = inputString.charAt(count);
                    cR--;
                }
                else if (r % 2 != 0 && count >= wordLen) {
                    charArray[r][cR] = '*';
                    cR--;
                }
                count++;
            }
        }
        for (int c = 0; c < columns; c++) {
            for (int r = 0; r < rows; r++) {
                System.out.print(charArray[r][c]);
            }
        }
        scanner.close();
    }
}
