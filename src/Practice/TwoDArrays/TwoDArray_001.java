package Practice.TwoDArrays;
import java.util.Scanner;


// write a program with a 2 x 3 two dimensional array
// that will receive input from the user and output
// values in row major order
// ex: input is: 1 2 3 4 5 6
// ex: output is: 1 2 3
//                4 5 6


public class TwoDArray_001 {
    public static void main(String[] arg) {
        Scanner scnr = new Scanner(System.in);
        System.out.println("Enter 6 numbers separated by spaces.");
        int[][] intArray = new int[2][3];

        for (int row = 0; row < 2; row++) {
            for (int col = 0; col < 3; col++) {
                intArray[row][col] = scnr.nextInt();
                System.out.print(intArray[row][col] + " ");
            }
            System.out.println();
        }
        scnr.close();
    }
}
