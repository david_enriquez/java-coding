package Practice.TwoDArrays;
import java.util.Scanner;
import java.io.*;


// write a program that reads text from a file create a
// two-dimensional character array that is rows * columns fill
// the array as follows:
// on even number rows, fill the row from the leftmost column to the rightmost
// on odd number rows, fill the row from the rightmost column to the leftmost
// fill any unused spaces in the two-dimensional array with the
// " * " character, if you have more characters than space, ignore
// the excess characters
// extract the chars from the array in column-major order
// (pull from column 0 first, then column 1, etc.) build a new
// string as you extract the chars, display the new string
// if the input string is "And the rest is rust and stardust" the
// output should be "A rsnusuudisd trtt ahsateens rd" another example
// if the input is "And the rest is" the output should be
// "A **ns**di** **tt**hs**ee** r**"


public class TwoDArray_004 {
    public static void main(String[] args) {
        // declare and initialize integer variables for the
        // number of rows and columns this determines the size
        // of the 2D character array "charArray"
        int rows = 4;
        int columns = 8;

        // create a 2D character array of size rows by columns "charArray"
        char[][] charArray = new char[rows][columns];

        // declare a Scanner object named "scanner" to read user input
        // initially set to read from "System.in"
        Scanner scanner = new Scanner(System.in);

        // here we use a try-catch block to attempt to open and read from
        // the file named "two_D_text.in." if the file is not found,
        // it will print a stack trace.
        try {
            scanner = new Scanner(new File("two_D_text.in"));
        }
        // the e.printStackTrace(); statement inside the catch block is
        // used to print the stack trace of the exception to the console this
        // stack trace includes information about which methods were called and
        // in what order leading up to the exception it can be very helpful for
        // diagnosing issues because it provides a detailed trace of how the
        // program reached the point where the error occurred so when the code
        // encounters a FileNotFoundException, it will print a stack trace
        // to the console, helping developers identify the location and context
        // of the error in their code.
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // here we read a line from the file using scanner.nextLine()
        // this means it reads the first line of text from the file "two_D_text.in."
        // we then assign the read line to the inputString variable so,
        // inputString now contains the content of the first line of the file.
        String inputString = scanner.nextLine();

        // declare an integer variable wordLen and assign the length of the string to it
        // we use this wordLen variable which calculate the length of the inputString
        // and store it in wordLen variable
        int wordLen = inputString.length();

        // declare an integer variable count to track the position or current letter
        // in the string
        int count = 0;

        // begin a nested loop the outer loop iterates over the rows (r), and the
        // inner loop iterates over the columns (c)
        for (int r = 0; r < rows; r++) {
            // declare integer variable cR and initialize it with the number of columns - 1
            int cR = columns - 1;

            // for each column c in the row (inner for loop that represents columns)
            for (int c = 0; c < columns; c++) {
                // if the current row r is even and count is less than wordLen
                if (r % 2 == 0 && count < wordLen) {
                    // assign to the current charArray element the current letter using the str.charAt(count) method
                    charArray[r][c] = inputString.charAt(count);
                }
                // else if the current row r is even and count is greater than or equal to wordLen
                else if (r % 2 == 0 && count >= wordLen) {
                    // assign to charArray element the "*" character
                    charArray[r][c] = '*';
                }
                // else if the current row r is odd and count is less than wordLen
                else if (r % 2 != 0 && count < wordLen) {
                    // using cR to represent the current column, assign to the current charArray element
                    // the current letter using the str.charAt(count) method
                    charArray[r][cR] = inputString.charAt(count);
                    // subtract 1 from the cR variable
                    cR--;
                }
                // else if the current row r is odd and count is greater than or equal to wordLen
                else if (r % 2 != 0 && count >= wordLen) {
                    // using cR to represent the current column, assign to charArray element the "*" character
                    charArray[r][cR] = '*';
                    // subtract 1 from the cR variable
                    cR--;
                }
                // add one to the count variable
                count++;
            }
        }

        // Output with nested for loops to output the contents of charArray
        // on a single line of text, in column-major order
        for (int c = 0; c < columns; c++) {
            for (int r = 0; r < rows; r++) {
                System.out.print(charArray[r][c]); // Print each character in column-major order
            }
        }
        scanner.close();
    }
}
