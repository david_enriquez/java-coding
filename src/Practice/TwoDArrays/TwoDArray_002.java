package Practice.TwoDArrays;
import java.util.Scanner;


// write a program with a 2 x 3 two dimensional
// array that will receive input from the user
// and output the values of each row in reverse
// (bottom to top) if the input is: 1 2 3 4 5 6
// the output should be
// 4 5 6
// 1 2 3


public class TwoDArray_002 {
    public static void main(String[] arg) {
        // create an instance of the Scanner class
        // named scnr this is used to read input from
        // the standard input stream (usually the keyboard)
        Scanner scnr = new Scanner(System.in);
        // display a message to the console asking the user
        // to enter 6 numbers separated by spaces
        System.out.println("Enter 6 numbers separated by spaces.");
        // this line declares a 2x3 dimensional integer array
        // named intArray it will be used to store the users input
        int[][] intArray = new int[2][3];

        // these nester for loops are used to read user input and
        // populate the intArray the outer loop iterates over rows
        // (0 and 1) of intArray
        for (int row = 0; row < 2; row++) {
            // the inner loop iterates over the columns (0, 1, and 2)
            // of intArray
            for (int col = 0; col < 3; col++) {
                // inside the inner loop, it uses scnr.nextInt()
                // to read an integer from the user and store it in
                // the appropriate position in intArray.
                intArray[row][col] = scnr.nextInt();
            }
        }
        // this line starts a for loop that iterates over the rows
        // of intArray in reverse order, from the last row (row 1)
        // to the first row (row 0)
        for (int row = 1; row >= 0; row--) {
            // inside the outer loop, this line starts another for
            // loop that iterates over the columns (0, 1, and 2) of
            // intArray
            for (int col = 0; col < 3; col++) {
                // inside the inner loop, it prints each element of
                // intArray followed by a space.
                System.out.print(intArray[row][col] + " ");
            }
            // after printing all elements in a row, this line uses
            // System.out.println() to move to the next line for the
            // next row.
            System.out.println();
        }
        scnr.close();
    }
}
