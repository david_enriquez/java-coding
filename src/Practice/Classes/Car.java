package Practice.Classes;

public class Car {
    private String make;
    private String model;

    public Car(String make, String model) {
        this.make = make;
        this.model = model;
    }

    public void startEngine() {
        System.out.println("The " + make + " " + model + " engine is running.");
    }

    public static void main(String[] args) {
        // Create a Car object
        Car myCar = new Car("Toyota", "Camry");

        // Identify the class of the object
        Class<?> carClass = myCar.getClass();

        // Output the class name
        System.out.println("The object was instantiated from the class: " + carClass.getName());

        // Use the object's method
        myCar.startEngine();
    }
}
