package Practice.Classes;

// info about a restaurant
public class Restaurant {
    private String name;
    private int rating;

    // sets the restaurants name
    public void setName(String restaurantName) {
        name = restaurantName;
    }

    // sets the rating (1-5, with 5 best)
    public void setRating(int userRating) {
        rating = userRating;
    }

    // prints name and rating on one line
    public void print() {
        System.out.println(name + " -- " + rating);
    }
}
