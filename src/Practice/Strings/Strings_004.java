package Practice.Strings;

import java.util.Scanner;

public class Strings_004 {
    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        String inputWord;
        int numLetters;
        int i;

        System.out.print("Enter a word: ");
        inputWord = scnr.next();

        numLetters = 0;
        scnr.close();
        for (i = 0; i < inputWord.length(); ++i) {
           if (Character.isLetter(inputWord.charAt(i))) {
              System.out.println(i);
              numLetters += 1;
           }
        }

        System.out.println("Number of letters: " + numLetters);
   }
}
