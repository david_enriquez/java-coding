package Practice.ArrayLists;
import java.util.ArrayList;

public class ArrayList_001 {
    public static void main(String[] args) {
        ArrayList<Integer> numbersList = new ArrayList<Integer>();
        numbersList.add(1);
        numbersList.add(2);
        numbersList.add(3);
        numbersList.add(4);
        numbersList.add(5);
        for (int num : numbersList) {
            System.out.println(num);
        }
    }
}
