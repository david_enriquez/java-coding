package Practice.Integers;
public class Integers_001 {
    public static void main(String[] args) {
        int product = 1;
        for (int i = 1; i < 3; i++) {
            System.out.println("outer loop: " + i);
            for (int j = 1; j < 3; j++) {
                product *= j;
                System.out.println("inner product: "+ product);
            }
        }
        System.out.print("outer product: " + product);
    }
}
