package Math;

public class Math_004 {
    public static void main(String[] args) {
        double x;

        x = Math.pow(2.0, 3.0);

        // This will output 1 decimal place
        System.out.printf("%.1f\n", x);
     }
}
