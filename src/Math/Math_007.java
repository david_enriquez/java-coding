package Math;
import java.util.Scanner;

// Compute: z = square root of y - x
public class Math_007 {
    public static void main(String[] args) {
      Scanner scnr = new Scanner(System.in);
      double x;
      double y;
      double z;

      x = scnr.nextDouble();
      y = scnr.nextDouble();

      z = Math.sqrt(y - x);


      System.out.printf("%.2f\n", z); // This will output only 2 decimal places.
      scnr.close();
    }
}
