package Math;

public class Math_001 {

    public static void mathExample001(String[] args) {
        double x = 9.0;
        double z = Math.pow(Math.sqrt(x) + Math.sqrt(x), 2.0);
        System.out.println("Example 001 Result: " + z); // Print the result
    }

    public static void mathExample002(String[] args) {
        double x = -9.0;
        double z = Math.sqrt(Math.abs(x));
        System.out.println("Example 002 Result: " + z);
    }

    public static void main(String[] args) {
        mathExample001(args);
        mathExample002(args);
    }
}
