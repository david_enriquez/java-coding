package Math;

public class Math_003 {
    public static void main(String[] args) {
        double x;

        x = Math.abs(-7.6);

        // This will output 1 decimal place
        System.out.printf("%.1f\n", x);
     }
}
