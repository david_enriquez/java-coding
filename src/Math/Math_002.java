package Math;

public class Math_002 {
    public static void main(String[] args) {
        double x;

        x = Math.sqrt(81.0);

        // This will output 1 decimal place
        System.out.printf("%.1f\n", x);
     }
}
