package Math;

public class Math_006 {
    public static void main(String[] args) {
        double x;
        System.out.println("Square Root of 9.0 is: " + Math.sqrt(9.0));
        x = Math.pow(2.0, Math.sqrt(9.0));

        // This will output 1 decimal place
        System.out.printf("%.1f\n", x);
     }
}
