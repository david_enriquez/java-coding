package TestQuestions;

public class testing18 {
    private double cost;

    public testing18(double c) {
        cost = c;
    }

    public void raiseCost(double c) {
        cost += c;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double c) {
        cost = c;
    }
}
class Driver{
    public static void main(String[] args) {
        testing18 stamp = new testing18(.35);
        stamp.raiseCost(.03);
        System.out.println("Stamp cost: " + stamp.getCost());
    }
}
