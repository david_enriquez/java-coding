package TestQuestions;

public class testing16 {
    public static void main(String[] args) {
        int num = 19283;
        while(mystery2(num) != 0) {
            System.out.print(mystery1(num));
            num = mystery2(num);
        }
    }
    public static int mystery2(int n) {
        return n/10;
    }
    public static int mystery1(int n) {
        return n%10;
    }
}
