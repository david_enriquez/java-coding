package JavaPrograms;
import java.util.Scanner;

public class GolfScore {
   public static void main(String[] args) {
      Scanner scnr = new Scanner(System.in);
      int numStrokes;

      numStrokes = scnr.nextInt();

      // Assumes "par 4"
      if (numStrokes <= 0) {
         System.out.println("Invalid entry.");
      }
      else if (numStrokes == 1) {
         System.out.println("Hole in 1!!!");
      }
      else if (numStrokes == 2) {
         System.out.println("Eagle!");
      }
      else if (numStrokes == 3) {
         System.out.println("Birdie.");
      }
      else if (numStrokes == 4) {
         System.out.println("Par.");
      }
      else {
         System.out.println("Better luck next time.");
      }
      scnr.close();
   }
}
