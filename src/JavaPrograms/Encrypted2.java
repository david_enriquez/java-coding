package JavaPrograms;
import java.io.*;
import java.util.Scanner;


public class Encrypted2 {
    public static void main(String[] args) {
        try {

            String strWord;
            String strUpperCaseWord;
            int evenOrOdd;

            PrintWriter pwtr = new PrintWriter("encrypted_results.out");
            Scanner scanner = new Scanner(new File("encrypted_input.in"));

            while (scanner.hasNext()) {

                strWord = scanner.next();
                strUpperCaseWord = strWord.toUpperCase();

                if (strWord.length() % 2 == 0) {
                    evenOrOdd = strWord.length() / 2;
                }

                else {
                    evenOrOdd = (strWord.length() + 1) / 2;
                }

                pwtr.printf("%-15s %s%n", strWord, strUpperCaseWord.substring(evenOrOdd) + strUpperCaseWord.substring(0, evenOrOdd));

            }

            scanner.close();
            pwtr.close();

            System.out.println("Encryption completed. Check 'encrypted_results.out' for results.");

        }
        catch (FileNotFoundException error) {

            System.err.println("File not found: " + error.getMessage());
        }
    }
}
