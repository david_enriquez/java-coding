package JavaPrograms;
import java.util.Scanner;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class ContributionTracker2 {
    public static void main(String[] args) throws FileNotFoundException {

        double numberOfContributions = 0;
        double maxContribution = 0;
        double minContribution = 10;
        double avgContribution = 0;
        double totalContribution = 0;
        boolean goalMet = false;

        FileOutputStream results = new FileOutputStream("results.out");
        PrintWriter pw = new PrintWriter(results);

        FileInputStream inputFromFile = new FileInputStream("input.in");
        Scanner scnr = new Scanner(inputFromFile);

        while (scnr.hasNextDouble()) {
            double contribution = scnr.nextDouble();
            if (contribution >= 10) {
                numberOfContributions++;
                totalContribution += contribution;
                if (contribution > maxContribution) {
                    maxContribution = contribution;
                }
                if (contribution < minContribution) {
                    minContribution = contribution;
                }
            }
            if (totalContribution >= 10_000_000 && !goalMet) {
                goalMet = true;
                pw.printf("It took %.0f contributions to reach the goal.%n", numberOfContributions);
            }
        }
        if (numberOfContributions > 0) {
            avgContribution = totalContribution / numberOfContributions;
        }
        if (!goalMet) {
            pw.println("Warning: The goal of $10,000,000 was not met.");
        }
        if (numberOfContributions == 0) {
            pw.println("Warning: The input file does not contain enough contributions.");
        }
        pw.printf("The maximum contribution received was $%.2f.%n", maxContribution);
        pw.printf("The minimum contribution received was $%.2f.%n", minContribution);
        pw.printf("The average contribution amount was $%.2f.%n", avgContribution);
        pw.printf("A total of $%.2f was collected.%n", totalContribution);
        scnr.close();
        pw.close();
    }
}
