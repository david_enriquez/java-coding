package JavaPrograms;
import java.util.Scanner;
import java.lang.Math;

public class GuessingGame2 {
    public static void main(String[] args) {
    int num = (int) (Math.round(Math.random() * 10000) + 1);
    int lowerRange = 0;
    int upperRange = 10000;
    Scanner scnr = new Scanner(System.in);
    int userGuess;
    while (true) {
        System.out.println("Enter your guess between " + lowerRange + " and " + upperRange + ": ");
        userGuess = scnr.nextInt();
        if (userGuess < num) {
            lowerRange = userGuess + 1;
            System.out.println("Please enter a number between " + lowerRange + " and " + upperRange);
            System.out.println("Your guess is to low, try again.");
        }
        else if (userGuess > num) {
            upperRange = userGuess - 1;
            System.out.println("Please enter a number between " + lowerRange + " and " + upperRange);
            System.out.println("Your guess is to high, try again.");
        }
        else {
            System.out.println("WINNER! The number was " + num);
            break;
        }

        }
        scnr.close();
    }
}
