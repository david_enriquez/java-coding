package JavaPrograms;
import java.util.Scanner;

public class TempConvert2 {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        System.out.print("Enter a Fahrenheit temperature: ");

        double fahrTemp = userInput.nextInt();
        double fahrConvtToCelsTemp;
        fahrConvtToCelsTemp = (fahrTemp - 32) * 5 / 9;

        System.out.printf(fahrTemp + "°F is equivalent to %f°C%n", fahrConvtToCelsTemp);
        System.out.print("Enter a Celsius temperature: ");

        double celsTemp = userInput.nextInt();
        double celsConvtToFahrTemp;
        celsConvtToFahrTemp = (celsTemp * 9/5) + 32;

        System.out.println(celsTemp + "°C is equivalent to " + celsConvtToFahrTemp + "°F");
        userInput.close();
    }
}
