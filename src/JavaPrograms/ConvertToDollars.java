package JavaPrograms;
import java.util.Scanner;

public class ConvertToDollars {
   public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Read the counts of quarters, dimes, nickels, and pennies as doubles
        double quarters = scanner.nextDouble();
        double dimes = scanner.nextDouble();
        double nickels = scanner.nextDouble();
        double pennies = scanner.nextDouble();
        double dollars;

        // Calculate the total amount using specified values
        quarters *= 0.25;
        dimes *= 0.10;
        nickels *= 0.05;
        pennies *= 0.01;
        dollars = quarters + dimes + nickels + pennies;

        // Output the result with two decimal places
        System.out.printf("Amount: $%.2f%n", dollars);

        scanner.close();
    }
}
