package JavaPrograms;
public class ShortCircuitEvaluation {
    public static void main(String[] args) {
        int x = 5;
        int y = 10;

        // Using logical AND (&&)
        if (x > 0 && y / x > 2) {
            // The right-hand side is not evaluated because x > 0 is false,
            // and the entire expression will be false.
            System.out.println("This won't be printed.");
        } else {
            System.out.println("Short-circuit evaluation with && worked.");
        }

        // Using logical OR (||)
        if (x == 5 || y / x > 2) {
            // The right-hand side is not evaluated because x == 5 is true,
            // and the entire expression will be true.
            System.out.println("Short-circuit evaluation with || worked.");
        } else {
            System.out.println("This won't be printed.");
        }
    }
}
