package JavaPrograms;
public class DrivingAge {
   public static void main (String [] args) {
      int drivingYear;
      int drivingAge;
      int numStates;

      drivingYear = 2014;
      drivingAge = 18;
      numStates = 10;

      System.out.println("In " + drivingYear + ", the driving age is " + drivingAge + ".");
      System.out.println(numStates + " states have exceptions.");
   }
}
