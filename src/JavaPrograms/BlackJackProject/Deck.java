package JavaPrograms.BlackJackProject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Deck {
    private List<Card> cards;
    private int top;

    public Deck() {
        cards = new ArrayList<>();
        top = 0;
    }

    public void createDeck() {
        String[] suits = {"Hearts", "Diamonds", "Clubs", "Spades"};
        String[] values = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"};

        for (String suit : suits) {
            for (String value : values) {
                cards.add(new Card(suit, value));
            }
        }
    }

    public void shuffle() {
        Collections.shuffle(cards);
    }

    public Card deal() {
        if (top < cards.size()) {
            Card card = cards.get(top);
            top++;
            return card;
        } else {
            return null; // Deck is empty
        }
    }

    public int remainingCards() {
        return cards.size() - top;
    }

    public static void main(String[] args) {
    Deck deck = new Deck();
    deck.createDeck();
    deck.shuffle();

    Scanner scanner = new Scanner(System.in);

    System.out.print("Enter the number of cards to deal: ");
    int numCardsToDeal = scanner.nextInt();

    for (int i = 0; i < numCardsToDeal; i++) {
        Card dealtCard = deck.deal();
        if (dealtCard != null) {
            System.out.println("Dealt: " + dealtCard);
        } else {
            System.out.println("No more cards in the deck.");
            break;
        }
    }

    // Close the scanner when done
    scanner.close();
    }
}
