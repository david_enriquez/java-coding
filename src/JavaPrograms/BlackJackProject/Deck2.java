package JavaPrograms.BlackJackProject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

// Write a main program that will allow user input to tell the number of
// cards to deal from the deck. Then the program will deal that specific
// number of cards from the deck displaying the cards in a format
// to reflect what the card is.

// The Card class defines the individual
// cards, including their suit and value.
class Card {
    // Declares attributes suit and value to store
    // the card's suit and value.
    private String suit;
    private String value;

    // we define a constructor for the Card class that takes
    // suit and value as parameters and assigns them to the
    // corresponding attributes of the object being created.
    public Card(String suit, String value) {
        // Assign the passed 'suit' to the object's 'suit' attribute.
        // Assign the passed 'value' to the object's 'value' attribute.
        this.suit = suit;
        this.value = value;
    }
    // The toString method in the Card class is used to
    // convert a card into a human-readable string for printing.
    public String toString() {
        return value + " of " + suit;
    }
}

// This line defines the Deck2 class, which represents a
// deck of cards. It contains methods and attributes related to the deck.
public class Deck2 {
    // This line declares a private attribute named "cards", which is a
    // list of Card objects. This list will hold all the cards in the deck.
    private List<Card> cards;
    // This line declares a private attribute top, which represents the
    // index of the top card in the deck. It will be used to keep track of
    // the next card to deal.
    private int top;

    // This is the constructor for the Deck2 class. It initializes
    // the cards list and sets top to 0 when a Deck2 object is created.
    public Deck2() {
        cards = new ArrayList<>();
        top = 0;
    }

    // This method is used to populate the cards list with a
    // standard deck of 52 cards, including different suits and values.
    public void createDeck() {
        String[] suits = {"Hearts", "Diamonds", "Clubs", "Spades"};
        String[] values = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"};

        // This is the outer loop, and it iterates through an array called suits.
        // The suits array contains the four possible suits of a standard deck of playing
        // cards: "Hearts," "Diamonds," "Clubs," and "Spades." The loop iterates through
        // each of these suits one by one.
        for (String suit : suits) {
            // This is the inner loop, nested inside the outer loop. It iterates
            // through an array called values. The values array contains the possible
            // values for each card in a suit, such as "2," "3," "4," ..., "King,"
            // and "Ace." The inner loop iterates through each value for the current suit.
            for (String value : values) {
                // Inside the inner loop, a new Card object is created with
                // the current suit and value from the loops. This represents
                // an individual playing card with a specific suit and value.
                // suit is the current suit from the outer loop.
                // value is the current value from the inner loop.
                cards.add(new Card(suit, value)); // new Card(suit, value) creates a new Card object with the current suit and value.
                // The newly created Card object is added to the cards list.
                // This process repeats for all possible combinations of suits and values,
                // resulting in a total of 52 cards in the cards list.
            }
        }
    }

    // This method shuffles the cards list to randomize the order of the cards.
    public void shuffle() {
        // We use the Collections class in the java.util package that provides
        // utility methods for working with collections, including lists.
        // shuffle(cards) is a static method from the Collections class.
        // It takes a list and rearranges its elements in a random order.
        Collections.shuffle(cards);
    }

    // This method deals the next card from the deck. If there are no
    // more cards to deal, it returns null.
    public Card deal() {
        // We check if there are remaining cards in the
        // deck (if top is less than the number of cards).
        if (top < cards.size()) {
            // If there are remaining cards, get the card at the current 'top' index.
            Card card = cards.get(top);
            // Increment the 'top' index to indicate that a card has been dealt.
            top++;
            // Return the card that has been dealt.
            return card;
        } else {
            // We display a message when the deck is empty.
            System.out.println("Sorry, the deck is empty. We need to reshuffle.");
            // If there are no more remaining cards, return null to indicate that the deck is empty.
            return null; // Deck is empty
        }
    }

    //This method calculates and returns the number of cards remaining in the deck.
    public int remainingCards() {
        return cards.size() - top;
    }

    // This is the entry point of the program. It's the main method
    // where the program execution begins.
    public static void main(String[] args) {
        // Here, we create a Deck2 object named deck.
        Deck deck = new Deck();
        // Here we call the createDeck method to create a standard deck of cards.
        deck.createDeck();
        // We shuffle the deck to randomize the order of the cards.
        deck.shuffle();

        // We create a Scanner object to read user input from the console.
        Scanner scanner = new Scanner(System.in);
        // We prompt the user to enter the number of cards they want to deal.
        System.out.print("Enter the number of cards to deal: ");
        // We read the user's input, which is the number of cards to deal.
        int numCardsToDeal = scanner.nextInt();

        // A loop from here on deals the specified number of cards and prints
        // the dealt cards. It checks if there are still cards to deal.
        for (int i = 0; i < numCardsToDeal; i++) {
            // Within each iteration, the 'deck.deal()' method is called
            // to deal a card from the deck. The dealt card is assigned
            // to the 'dealtCard' variable.
            Card dealtCard = deck.deal();
            // We check if a card was successfully dealt.
            if (dealtCard != null) {
                // If a card was successfully dealt, it prints a message indicating the dealt card.
                System.out.println("Dealt: " + dealtCard);
            } else {
                // If 'dealtCard' is null, it means the deck is empty, so it prints
                // a message indicating that there are no more cards in the deck.
                System.out.println("No more cards in the deck.");
                break;
            }
        }

        // Close the scanner when done
        scanner.close();
    }
}
