package JavaPrograms.BlackJackProject;
import java.util.Scanner;

public class BlackJack {
    public static void main(String[] args) {
        Deck deck = new Deck();
        deck.createDeck();
        deck.shuffle();

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the number of cards to deal: ");
        int numCardsToDeal = scanner.nextInt();

        for (int i = 0; i < numCardsToDeal; i++) {
            Card dealtCard = deck.deal();
            if (dealtCard != null) {
                System.out.println("Dealt: " + dealtCard);
            } else {
                System.out.println("No more cards in the deck.");
                break;
            }
        }

        // Close the scanner when done
        scanner.close();
    }
}
