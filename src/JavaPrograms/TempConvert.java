package JavaPrograms;
import java.util.Scanner;

public class TempConvert {
    public static void main(String[] args) {
        // "Scanner" object named userInput to read user input
        Scanner userInput = new Scanner(System.in);

        // prompt user to input a fahrenheit temp
        System.out.print("Enter a Fahrenheit temperature: ");

        // read the int which the user input and assign the
        // int to a double variable so we can store floating-point
        // values for temp
        double fahrTemp = userInput.nextInt();

        // declare a double variable to store the converted
        // temp in celsius
        double fahrConvtToCelsTemp;

        // calculate the temp conversion from °F to °C
        fahrConvtToCelsTemp = (fahrTemp - 32) * 5 / 9;

        // printf to format and display the converted temp
        // also using these format specifiers %f indicates a floating-point
        // num should be inserted then °C is literal string then %n
        // adds a new line character
        System.out.printf(fahrTemp + "°F is equivalent to %f°C%n", fahrConvtToCelsTemp);

        // prompt user to input a celsius temp
        System.out.print("Enter a Celsius temperature: ");

        // read the int which the user input and assign the
        // int to a double variable so we can store floating-point
        // values for temp
        double celsTemp = userInput.nextInt();

        // declare a double variable to store the converted
        // temp in celsius
        double celsConvtToFahrTemp;

        // calculate the temp conversion from °C to °F
        celsConvtToFahrTemp = (celsTemp * 9/5) + 32;

        // display the celsius temp converted to fahrenheit temp
        System.out.println(celsTemp + "°C is equivalent to " + celsConvtToFahrTemp + "°F");

        userInput.close();
    }
}
