package JavaPrograms;
import java.util.Scanner;

public class StatisticsCalculator {
   public static void main(String[] args) {
      Scanner input = new Scanner(System.in);

        int total = 0;
        int count = 0;
        int maximum = -1;

        while (true) {
            int num = input.nextInt();

            if (num < 0) {
                break;
            }

            total += num;
            count++;

            if (num > maximum) {
                maximum = num;
            }
        }

        if (count > 0) {
            double average = (double) total / count;
            System.out.println(maximum + " " + (int) Math.round(average));
        } else {
            System.out.println("No valid input provided.");
        }

        input.close();
   }
}
