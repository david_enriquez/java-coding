package JavaPrograms;
// import "Scanner" class from java package this allows
// us to read input from the user
import java.util.Scanner;

public class SumOfFiveDigits {
    // Write a program that prompts the user to enter a 5-digit
    // positive integer. Using only the / and % operations,
    // compute each individual digit and display the sum of the digits.
    // Here is a sample run with the user input underlined.
    // Enter a 5-digit positive integer: 30458
    // The sum of the digits is 3 + 0 + 4 + 5 + 8 = 20

    // main() is the beginning of our program
    public static void main(String[] args) {
        // we name a "Scanner" object userInput which allows
        // us to read the users input
        Scanner userInput = new Scanner(System.in);

        // this line prompts the user to enter a positive 5 digit num
        System.out.print("Enter a positive 5-digit number: ");

        // we store the users input into the integer variable
        // int and this variable is named positiveFiveDigitNum
        int positiveFiveDigitNum = userInput.nextInt();

        // declare and initialize int variables to store
        // each digit along with a sum variable to store the sum
        // of the digits
        int num1 = 0;
        int num2 = 0;
        int num3 = 0;
        int num4 = 0;
        int num5 = 0;
        int sum = 0;

        // extract and sum each individual digit using / and %
        // we extract the digit furthest to the left
        num1 = positiveFiveDigitNum / 10000;

        // console log the digit furthest to the left
        // System.out.println(num1);

        // we remove the digit furthest to the left
        positiveFiveDigitNum %= 10000;

        // console log to be sure the digit was removed
        // System.out.println(positiveFiveDigitNum);

        // we extract the digit furthest to the left
        // since we know there are only four digits left, we use 1000
        num2 = positiveFiveDigitNum / 1000;

        // console log the digit furthest to the left
        // System.out.println(num2);

        // we remove the digit furthest to the left
        positiveFiveDigitNum %= 1000;

        // console log to be sure the digit was removed
        // System.out.println(positiveFiveDigitNum);

        // we extract the digit furthest to the left
        // since we know there are only four digits left, we use 100
        num3 = positiveFiveDigitNum / 100;

        // console log the digit furthest to the left
        // System.out.println(num3);

        // we remove the digit furthest to the left
        positiveFiveDigitNum %= 100;

        // console log to be sure the digit was removed
        // System.out.println(positiveFiveDigitNum);

        // we extract the digit furthest to the left
        // since we know there are only four digits left, we use 10
        num4 = positiveFiveDigitNum / 10;

        // we extract the digit furthest to the left, in this case
        // it is the last number input when you perform a modulo 10
        // operation on a single number, you will get that single
        // number 5 % 10 is 5, after attempting to do 5 divided by
        // 10 you get a fraction which in this case the remainder and is
        // the single digit remaining
        num5 = positiveFiveDigitNum % 10;

        // we then calculate the sum of each individual num and
        // store the sum in the sum variable
        sum = num1 + num2 + num3 + num4 + num5;

        // format the output display
        System.out.println("The sum of all of the digits is " + num1 + " + " + num2 + " + " + num3 + " + " + num4 + " + " + num5 + " = " + sum);

        userInput.close();
    }
}
