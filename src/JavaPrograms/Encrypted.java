package JavaPrograms;
import java.io.*;
import java.util.Scanner;


public class Encrypted {
    public static void main(String[] args) {
        // we use a try-catch block to handle potential exceptions
        // such as file not found errors.
        try {
            // variable strWord will store the original word from
            // the input file.
            String strWord;
            // variable strUpperCaseWord will store the uppercase
            // version of strWord.
            String strUpperCaseWord;
            // variable evenOrOdd will store the value of the length
            // of the strWord for encryption. calculates
            // the evenOrOdd value based on whether
            // the length of the strWord is even or odd
            int evenOrOdd;

            // create PrintWriter object to write to "encrypted_results.out"
            PrintWriter pwtr = new PrintWriter("encrypted_results.out");

            // create Scanner object to read words from "encrypted_input.in"
            Scanner scanner = new Scanner(new File("encrypted_input.in"));

            // enter a while loop, which will continue as long as
            // there are more words to read in the encrypted_input file
            while (scanner.hasNext()) {
                // next() method of the Scanner object reads the next word from
                // the input file and stores it in the strWord variable.
                strWord = scanner.next();
                // toUpperCase() method is used to convert the strWord to uppercase
                // and store it in strUpperCaseWord.
                strUpperCaseWord = strWord.toUpperCase();

                // this block of code calculates the evenOrOdd value based on whether
                // the length of the strWord is even or odd. The evenOrOdd will be
                // used for encryption.
                if (strWord.length() % 2 == 0) {
                    evenOrOdd = strWord.length() / 2;
                } else {
                    evenOrOdd = (strWord.length() + 1) / 2;
                }

                // In this code, we use pwtr.printf() to format the output. the format
                // string "%-15s %s%n" specifies the formatting: %-15s: This formats
                // the first string (strWord) as left-justified within a 15-character-wide column.
                // It ensures that each word takes up at least 15 characters of space. %s: This formats
                // the second string (strUpperCaseWord.substring(mid) + strUpperCaseWord.substring(0, mid)) as is.
                // %n: This represents a platform-independent newline character to move to the next line.
                // This formatting ensures that the words and their encrypted equivalents are aligned
                // in columns with consistent spacing, as shown in your desired output.
                // we can adjust the 15 in %-15s to control the width of the columns as needed.
                pwtr.printf("%-15s %s%n", strWord, strUpperCaseWord.substring(evenOrOdd) + strUpperCaseWord.substring(0, evenOrOdd));
            }

            // after processing all words in the input file, the program closes both
            // the Scanner and PrintWriter objects.
            scanner.close();
            pwtr.close();

            // we prints a message to the console indicating that the encryption process has
            // completed and suggests checking the "output.out" file for results.
            System.out.println("Encryption completed. Check 'encrypted_results.out' for results.");

            // we handle a FileNotFoundException if the input file ("input.in") is not
            // found and prints an error message indicating which file is missing.
        } catch (FileNotFoundException error) {
            System.err.println("File not found: " + error.getMessage());
        }
    }

}
