package JavaPrograms;
import java.util.Scanner;

// Write a program that reads a list of words.
// Then, the program outputs those words and their
// frequencies. The input begins with an integer
// indicating the number of words that follow.
// Assume that the list will always contain less
// than 20 words. Ex: If the input is: 5 hey hi Mark hi mark
// the output is:
// hey 1
// hi 2
// Mark 1
// hi 2
// mark 1
// Hint: Use two arrays, one for the strings, another
// for the frequencies. Your program must define
// and call a method: public static int
// getFrequencyOfWord(String[] wordsList, int listSize,
// String currWord)

public class WordFreqs {

    // This method is used to count the frequency of a specific word in an array of words.
    // It takes three parameters: wordsList: An array of words. listSize: The number
    // of elements in the array. currWord: The word you want to count the
    // frequency of. Inside the method: frequency is initialized to 0, which
    // will be used to store the word's frequency. A for loop iterates through
    // the wordsList array. If the current word in the array (wordsList[i]) is
    // equal to the currWord, the frequency is incremented by 1. Finally, the method
    // returns the calculated frequency.
    public static int getFrequencyOfWord(String[] wordsList, int listSize, String currWord) {
        int frequency = 0;
        for (int i = 0; i < listSize; i++) {
            if (wordsList[i].equals(currWord)) {
                frequency++;
            }
        }
        return frequency;
    }

    public static void main(String[] args) {
        // Here, a Scanner object scnr is created to read input from the user.
        Scanner scnr = new Scanner(System.in);

        // Number of words in user's list
        int numWords = scnr.nextInt();

        // Array to store the user's input
        // An array wordsList is created to store the words that the user will
        // input. The size of the array is determined by numWords.
        String[] wordsList = new String[numWords];

        // Read the words into the array
        // A for loop is used to read words from the user and store them in the wordsList array.
        for (int i = 0; i < numWords; i++) {
            wordsList[i] = scnr.next();
        }

        // Output the words and their frequencies
        // Another for loop iterates through the wordsList array.
        // For each word, it calls the getFrequencyOfWord method to count
        // its frequency, and then it prints the word and its frequency
        // to the console.
        for (int i = 0; i < numWords; i++) {
            String word = wordsList[i];
            int frequency = getFrequencyOfWord(wordsList, numWords, word);
            System.out.println(word + " " + frequency);
        }

        // Close the scanner
        scnr.close();
    }
}
