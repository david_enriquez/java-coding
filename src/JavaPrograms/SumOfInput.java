package JavaPrograms;

import java.util.Scanner;

public class SumOfInput {

    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);

        System.out.print("Enter a positive 5-digit number: ");

        int positiveFiveDigitNum = userInput.nextInt();
        int num1 = 0;
        int num2 = 0;
        int num3 = 0;
        int num4 = 0;
        int num5 = 0;
        int sum = 0;

        num1 = positiveFiveDigitNum / 10000;
        positiveFiveDigitNum %= 10000;
        num2 = positiveFiveDigitNum / 1000;
        positiveFiveDigitNum %= 1000;
        num3 = positiveFiveDigitNum / 100;
        positiveFiveDigitNum %= 100;
        num4 = positiveFiveDigitNum / 10;
        num5 = positiveFiveDigitNum % 10;
        sum = num1 + num2 + num3 + num4 + num5;

        System.out.println("The sum of all of the digits is " + num1 + " + " + num2 + " + " + num3 + " + " + num4 + " + " + num5 + " = " + sum);
        userInput.close();
    }
}
