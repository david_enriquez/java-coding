package JavaPrograms;
import java.util.Scanner;
import java.lang.Math;

public class GuessingGame {
    public static void main(String[] args) {
    // Math.random() generates a random decimal number
    // between 0 (inclusive) and 1 (exclusive) we multiply
    // the random decimal by 10000 to get a random decimal
    // between 0 and 10000 Math.round(...) rounds the decimal
    // number to the nearest integer + 1, we add 1 to the rounded
    // number to shift the range from [0, 9999] to [1, 10000].
    int num = (int) (Math.round(Math.random() * 10000) + 1);

    // declare two variables for the upper and lower range
    // of users guess and initialize examples are
    // lowerRange = 0 and upperRange = 10000
    int lowerRange = 0;
    int upperRange = 10000;

    // user input with Scanner obj
    Scanner scnr = new Scanner(System.in);

    // initialize a variable for the users guess from their input
    int userGuess;

    // Repeat with a loop until the user's guess is correct
    while (true) {
        // Prompt the user for their guess
        System.out.println("Enter your guess between " + lowerRange + " and " + upperRange + ": ");
        userGuess = scnr.nextInt();

        // check if the user's guess is less than the random number
        // if users guess is less than the random number
        // lowerRange = the users guess + 1
        // System.out.println("LOWER, between "+ lowerRange + "and" + upperRange)
        if (userGuess < num) {
            lowerRange = userGuess + 1;
            System.out.println("Please enter a number between " + lowerRange + " and " + upperRange);
            System.out.println("Your guess is to low, try again.");
        }

        // else if the users guess is greater than random number
        // upperRange = the users guess - 1
        // System.out.println("HIGHER, between " + lowerRange + "and" + upperRange)
        else if (userGuess > num) {
            upperRange = userGuess - 1;
            System.out.println("Please enter a number between " + lowerRange + " and " + upperRange);
            System.out.println("Your guess is to high, try again.");
        }

        // else if the user's guess is correct System.out.println("WINNER")
        else {
            System.out.println("WINNER! The number was " + num);
            // exit the loop
            break;
        }

        }
        scnr.close();
    }
}
