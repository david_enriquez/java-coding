package JavaPrograms;
// I have learned that whatever you name your file,
// you must also create a class named the same as your file.
// declared a class called Introduction
public class Introduction {
    // the main() is the starting point of the program
    public static void main(String [] args) {
        // System.out.println() method to print the line of text to console
        System.out.println("My name is David");
        // System.out.println() method to print the line of text to console
        System.out.println("My favorite color is blue.");
        // System.out.println() method to print the line of text to console
        System.out.println("If I won the lottery, I would invest and travel the world.");
     }
}
