package JavaPrograms;
import java.util.Scanner;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class ContributionTracker {
    public static void main(String[] args) throws FileNotFoundException {

        // initialize variables
        double numberOfContributions = 0; // counts total num of contributions
        double maxContribution = 0; // this variable holds the maximum contribution
        double minContribution = 10; // set minimum contribution to 10
        double avgContribution = 0; // this variable stores the average contribution
        double totalContribution = 0; // this variable increments the total contribution amount
        // added boolean variable named goalMet to handle the
        // case of input file not containing enough contributions
        // to meet the goal
        boolean goalMet = false;

        // create a "FileOutputStream" and "PrintWriter" for writing to results.out
        FileOutputStream results = new FileOutputStream("results.out");
        PrintWriter pw = new PrintWriter(results);

        // create a "FileInputStream" and "Scanner" for reading from input.in
        // read data from an input file named "input.in"
        // a Scanner object named scnr to read data from the input file.
        FileInputStream inputFromFile = new FileInputStream("input.in");
        Scanner scnr = new Scanner(inputFromFile);

        // read data from input.in and process it using a while loop which
        // will continue as long as there are more double values in
        // the input file (hasNextDouble()).
        while (scnr.hasNextDouble()) {
            // read the next contribution from the input file
            double contribution = scnr.nextDouble();

            // check if the contribution is at least $10
            if (contribution >= 10) {
                // increment the number of contributions
                numberOfContributions++;

                // update the total contribution
                totalContribution += contribution;

                // update the maximum contribution if necessary
                if (contribution > maxContribution) {
                    maxContribution = contribution;
                }

                // update the minimum contribution if necessary
                if (contribution < minContribution) {
                    minContribution = contribution;
                }
            }

            // check if the total contribution reaches or exceeds $10,000,000
            // if this goal is met and goalMet is still false, it sets goalMet
            // to true and prints the message about the number of contributions
            // needed to reach the goal. This message is only printed once when
            // the goal is first met.
            if (totalContribution >= 10_000_000 && !goalMet) {
                goalMet = true;
                pw.printf("It took %.0f contributions to reach the goal.%n", numberOfContributions);
            }
        }

        // after running all lines in the file calculate the average contribution
        if (numberOfContributions > 0) {
            avgContribution = totalContribution / numberOfContributions;
        }

        // after running all lines in the file if goalMet is still false
        if (!goalMet) {
            pw.println("Warning: The goal of $10,000,000 was not met.");
        }

        // if the file has zero contributions output this
        if (numberOfContributions == 0) {
            pw.println("Warning: The input file does not contain enough contributions.");
        }

        // printf to format and display the amount
        // also using these format specifiers 2f indicates a floating-point decimal
        // with two places
        pw.printf("The maximum contribution received was $%.2f.%n", maxContribution);
        pw.printf("The minimum contribution received was $%.2f.%n", minContribution);
        pw.printf("The average contribution amount was $%.2f.%n", avgContribution);
        pw.printf("A total of $%.2f was collected.%n", totalContribution);

        // close the input and output streams
        scnr.close();
        pw.close();



    }
}
