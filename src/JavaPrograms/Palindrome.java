package JavaPrograms;
import java.util.Scanner;

public class Palindrome {
    public static void main (String[] args) {
        //check if a word input is palindrome
        Scanner userInput = new Scanner(System.in);

        //get input word from user
        System.out.print("Enter a word: ");
        String userWord = userInput.nextLine();

        //remove spaces and convert to lowercase
        userWord = userWord.replaceAll(" ", "").toLowerCase();

        //declare and initialize three variables
        //left is initialized to 0representing the
        //leftmost character index of the "userWord" string
        int left = 0;

        //right is initialized to the index of the rightmost
        //character in the "userWord" string
        int right = userWord.length() - 1;

        //isPalindrome variable is initialized to true, assuming
        //the "userWord" is initially a palindrome
        boolean isPalindrome = true;
        userInput.close();

        //this line starts a while loop that continues as
        //long as the left index is less than the right index.
        //the loop iterates from both ends of the string towards
        //the center to check for palindrome properties.
        while (left < right) {

            //inside the loop, it checks if the character at the
            //left index does not match the character at the right
            //index. If they don't match, it sets isPalindrome to
            //false, indicating that the word is not a palindrome,
            //and then breaks out of the loop because there's no need
            //to continue checking.
            if (userWord.charAt(left) != userWord.charAt(right)) {
                System.out.println("Inside the while loop at [left]..." + userWord.charAt(left));
                System.out.println("Inside the while loop at [right]..." + userWord.charAt(right));
                isPalindrome = false;
                System.out.println(isPalindrome);
                break; // no need to continue checking
            }

            //after each character comparison, both left and right
            //indices are updated to move towards the center of the
            //string.
            left++;
            right--;
            System.out.println("This is left [" + left + "] index incremented");
            System.out.println("This is right [" + right + "] index decremented");
        }

        if (isPalindrome) {
            System.out.println(userWord + " is a palindrome!");
        }
        else {
            System.out.println(userWord + " is NOT a palindrome!");
        }
    }
}
